import React from "react";
import './App.css'

import Card from "./layouts/Card";
import Product from "./components/Product";

export default props => (
    <div className="App">
        <Card titulo="PRODUTO 1">
            <Product titulo="Calça"/>
        </Card>
        <Card titulo="PRODUTO 2">
            <Product titulo="Calça"/>
        </Card>
        <Card titulo="PRODUTO 3">
            <Product titulo="Calça"/>
        </Card>
        <Card titulo="PRODUTO 4">
            <Product titulo="Calça"/>
        </Card>
        <Card titulo="PRODUTO 5">
            <Product titulo="Calça"/>
        </Card>
    </div>
);