import React from "react";

import Pants from './imgs/pants.png'

export default props => (
    <div>
        <h3>{props.titulo}</h3>
        <img src={Pants} alt="Calça"/>
    </div>
);