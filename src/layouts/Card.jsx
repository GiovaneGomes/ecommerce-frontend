import React from "react";
import './Card.css'

export default props => (
    <div className="Card">
        <div className="Content">
            {props.children}
        </div>
        <button className="Footer" onClick={handleClick}>
                {props.titulo}
        </button>
    </div>
);

const handleClick = () => {
    console.log('Show product')
}